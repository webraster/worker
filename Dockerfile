FROM alpine:edge
MAINTAINER voidreturn

RUN apk update && apk upgrade && \
    apk add --no-cache chromium nss freetype freetype-dev harfbuzz ttf-freefont && \
    apk add --no-cache nodejs-npm python make g++

COPY . /src
WORKDIR /src

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

RUN npm ci

CMD ["node", "/src"]
