#!/usr/bin/env node

/**
 * Bootstrap a process listening to a redis server to create browser screenshots
 */

require('dotenv').config()

const jimp = require('jimp')
const AWS = require('aws-sdk')
const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.SPACES_KEY,
    secretAccessKey: process.env.SPACES_SECRET,
  },
  endpoint: 'sfo2.digitaloceanspaces.com',
})
const URL = require('url')
const fs = require('fs')
const puppeteer = require('puppeteer')
const redis = require('redis')
const IMAGE_QUEUE_KEY = 'url_image_requests'
const MAX_PARALLEL = process.env.MAX_PARALLEL || 10

const client = redis.createClient({ url: process.env.REDIS_URL })
const sub = redis.createClient({ url: process.env.REDIS_URL })

client.on('ready', async () => {
  console.log('redis connected')
  runJob()
})

client.on('error', (err) => {
  console.log('Error from redis client', err)
})

sub.on('message', (channel, message) => {
  runJob()
})

sub.subscribe(IMAGE_QUEUE_KEY, (err, count) => {
  if (err) return console.log('Error subscribing', err)
  console.log('subscribed to channel', count)
})

function urlFilename(url) {
  const parsed = URL.parse(url)
  return `${parsed.protocol}-${parsed.hostname}-${parsed.port}-${parsed.pathname}.jpg`
}

let serialQueues = Array.apply(null, Array(MAX_PARALLEL)).map(() => Promise.resolve())
let running = false
async function runJob() {
  if (running) return
  running = true
  try {
    serialQueues = serialQueues.map((queue) => queue.then(_runJob))
    await Promise.all(serialQueues)
  } catch {
    console.log('Error running job')
  }
  running = false
}

async function _runJob() {
  try {
    const job = await loadJob()
    if (!job) return
    if (!job.url) return
    const buffer = await screenshotBuffer(job)
    await new Promise((rs, rj) => {
      s3.putObject({
        Bucket: 'webraster',
        Key: urlFilename(job.url),
        Body: buffer,
        ACL: 'public-read',
      }, (err, data) => {
        if (err) return rj(err)
        rs()
      })
    })
    await new Promise((rs, rj) => {
      client.publish('url_image_completion', JSON.stringify({
        url: job.url,
        finalUrl: `https://webraster.sfo2.digitaloceanspaces.com/${urlFilename(job.url)}`,
      }), (err) => {
        if (err) return rj(err)
        rs()
      })
    })
    return job
  } catch (err) {
    console.log('Error running job', err)
  } finally {
    await _runJob()
  }
}

async function loadJob() {
  return await new Promise((rs, rj) => {
    client.rpop(IMAGE_QUEUE_KEY, (err, job) => {
      if (err) return rj(err)
      rs(job === null ? undefined : JSON.parse(job))
    })
  })
}

async function jobCount() {
  return await new Promise((rs, rj) => {
    client.llen(IMAGE_QUEUE_KEY, (err, count) => {
      if (err) return rj(err)
      rs(count)
    })
  })
}

let browser
let pageCount = 0
async function screenshotBuffer(options = {}) {
  if (pageCount > 50) {
    await browser.close()
    delete browser
  }
  if (!browser) {
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      // executablePath: await chrome.executablePath,
      // headless: chrome.headless,
      executablePath: '/usr/bin/chromium-browser',
    })
  }
  const { url: _url, resize } = options
  const parsed = URL.parse(_url)
  if (!parsed.protocol) {
    parsed.protocol = 'http:'
    parsed.slashes = true
  }
  const url = URL.format(parsed)
  console.log(`Screenshotting ${url}`)
  const width = 0
  const height = 0
  const DEFAULT_WIDTH = 900
  const DEFAULT_HEIGHT = 900
  try {
    const page = await browser.newPage()
    await page.setViewport({
      width: DEFAULT_WIDTH,
      height: DEFAULT_HEIGHT,
      deviceScaleFactor: 1,
    })
    await page.goto(url, {
      waitUntil : 'domcontentloaded',
    })
    await new Promise(r => setTimeout(r, 1000))
    const screenshotData = await page.screenshot({
      clip: { x: 0, y: 0, width: DEFAULT_WIDTH, height: DEFAULT_HEIGHT },
    })
    await page.close()
    pageCount += 1
    if (width && height) {
      const image = await jimp.read(screenshotData)
      image.resize(+width, +height)
      return await image.getBufferAsync('image/png')
    }
    return screenshotData
  } catch (err) {
    throw err
  }
}

process.on('uncaughtException', (err, origin) => {
  console.log('Uncaught exception')
  console.log(err)
  console.log(origin)
  console.log('Exiting process')
  process.exit(1)
})

process.on('unhandledRejection', (err, promise) => {
  console.log('Unhandled rejection in process, exiting')
  process.exit(1)
})
